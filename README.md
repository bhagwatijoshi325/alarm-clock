# Alarm-Clock using Vanilla JavaScript

<li>This is a repository for Alarm Clock Developed using Vanilla JavaScript, HTML, CSS.</li>
<br>
<li> It is developed to create a soothing Experience with powerful features and clean interface.</li>
<br>

# Problem Statement

Create an alarm clock. Use ONLY vanilla javascript, no libraries or frameworks allowed for Javascript (you can use any css framework like Bootstrap).
<br>

# Features Required

- <b>Clock face</b><br>

  Clock showing the current time (seconds,mins,hrs should change as time changes)

- <b>Set Alarm</b> <br>

  - Provide input boxes to set an alarm (hr,min,sec, am/pm)
  - Once the sets the time and click “Set Alarm” button, add that alarm to the alarms list below
  - When the alarm goes of just use JS alert function to alert in the browser

- <b>Alarms list</b> <br>

  - Display a list of all the alarms set by user
 
- <b>Delete alarm</b> <br>
  - For each alarm give a delete button to delete the alarm
  - When the user deletes an alarm make sure it “does not alerts the user”

<br>

## Technologies Employed

- Vanilla JavaScript (No libraries or frameworks allowed)
- HTML5
- CSS (Any CSS framework like Bootstrap can be used)

## Requirements

A modern web browser that supports HTML5, CSS3, and JavaScript.

## Installation and Running

1. Clone the GitHub repository:
   ```bash
   $ git clone https://gitlab.com/bhagwatijoshi325/alarm-clock
   ```
2. Open index.html file in a browser.


## Directory Structure
```
[ALARM-CLOCK]
│ index.html
│ README.md
│ index.js
│ style.css
```

## Support
Contributions, bug reports, and feature requests are welcome. Feel free to fork the repository and submit pull requests to contribute to the project.





 
