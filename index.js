// Selecting DOM elements
const currentTime = document.querySelector("#current-time");
const setHours = document.querySelector("#hours");
const setMinutes = document.querySelector("#minutes");
const setSeconds = document.querySelector("#seconds");
const setAmPm = document.querySelector("#am-pm");
const setAlarmButton = document.querySelector("#submitButton");
const alarmContainer = document.querySelector("#alarms-container");

// Event listener for DOMContentLoaded to initialize dropdown menus and set intervals
window.addEventListener("DOMContentLoaded", (event) => {
  dropDownMenu(1, 12, setHours); // Initialize hours dropdown
  dropDownMenu(0, 59, setMinutes); // Initialize minutes dropdown
  dropDownMenu(0, 59, setSeconds); // Initialize seconds dropdown
  setInterval(getCurrentTime, 1000); // Set interval to update current time
  fetchAlarm(); // Fetch alarms from local storage
});

// Event listener for setting alarm
setAlarmButton.addEventListener("click", getInput);

// Function to populate dropdown menu with given range
function dropDownMenu(start, end, element) {
  for (let i = start; i <= end; i++) {
    const dropDown = document.createElement("option");
    dropDown.value = i < 10 ? "0" + i : i;
    dropDown.innerHTML = i < 10 ? "0" + i : i;
    element.appendChild(dropDown);
  }
}

// Function to get and display current time
function getCurrentTime() {
  let time = new Date();
  time = time.toLocaleTimeString("en-US", {
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    hour12: true,
  });
  currentTime.innerHTML = time;
  return time;
}

// Function to get input values and set alarm
function getInput(e) {
  e.preventDefault();
  const alarmTime = convertToTime(
    setHours.value,
    setMinutes.value,
    setSeconds.value,
    setAmPm.value
  );
  setAlarm(alarmTime);
}

// Function to convert time to 24-hour format
function convertToTime(hour, minute, second, amPm) {
  return `${parseInt(hour)}:${minute}:${second} ${amPm}`;
}

// Function to set alarm and check if it's time to ring
function setAlarm(time, fetching = false) {
  const alarm = setInterval(() => {
    if ( time === getCurrentTime() ) {
      alert("Alarm Ringing");
    }
  }, 500);
  addAlaramToDom(time, alarm);
  if (!fetching) {
    saveAlarm(time);
  }
}

// Function to add alarm to DOM
function addAlaramToDom(time, intervalId) {
  const alarm = document.createElement("div");
  alarm.classList.add("alarm", "mb", "d-flex");
  alarm.innerHTML = `
              <div class="time">${time}</div>
              <button class="btn delete-alarm" data-id=${intervalId}>Delete</button>
              `;
  const deleteButton = alarm.querySelector(".delete-alarm");
  deleteButton.addEventListener("click", (e) => deleteAlarm(e, time, intervalId));
  alarmContainer.prepend(alarm);
}

// Function to check if alarms are saved in local storage
function checkAlarams() {
  let alarms = [];
  const isPresent = localStorage.getItem("alarms");
  if (isPresent) alarms = JSON.parse(isPresent);
  return alarms;
}

// Function to save alarm to local storage
function saveAlarm(time) {
  const alarms = checkAlarams();
  alarms.push(time);
  localStorage.setItem("alarms", JSON.stringify(alarms));
}

// Function to fetch alarms from local storage
function fetchAlarm() {
  const alarms = checkAlarams();
  alarms.forEach((time) => {
    setAlarm(time, true);
  });
}

// Function to delete alarm from DOM and clear interval
function deleteAlarm(event, time, intervalId) {
  clearInterval(intervalId);
  const self = event.target;
  const alarm = self.parentElement;
  deleteAlarmFromLocal(time);
  alarm.remove();
}

// Function to delete alarm from local storage
function deleteAlarmFromLocal(time) {
  const alarms = checkAlarams();
  const index = alarms.indexOf(time);
  alarms.splice(index, 1);
  localStorage.setItem("alarms", JSON.stringify(alarms));
}
